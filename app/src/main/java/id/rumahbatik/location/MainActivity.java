package id.rumahbatik.location;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class MainActivity extends ActionBarActivity {
    EditText editText;
    Location location;
    double latitude;
    double longitude;
    String namaJalan, namaKota, namaNegara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        switchGPS();
        editText = (EditText) findViewById(R.id.editTextResult);
        Button button = (Button) findViewById(R.id.buttonGetLocation);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });
    }

    void switchGPS() {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        intent.setData(Uri.parse("3"));
        sendBroadcast(intent);
    }

    void getLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new AppLocationListener();
        boolean gpsIsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean netIsEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gpsIsEnabled) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (netIsEnabled && location == null) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        } else {
            Location lastLocation = lastKnownLocation(this);
            if (lastLocation != null) {
                latitude = lastLocation.getLatitude();
                longitude = lastLocation.getLongitude();
            }
        }
        locationManager.removeUpdates(locationListener);
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            namaJalan = addresses.get(0).getAdminArea();
            Log.d("nama jalan:", namaJalan);
            namaKota = addresses.get(0).getLocality();
            Log.d("nama kota:", namaKota);
            namaNegara = addresses.get(0).getCountryName();
            Log.d("nama negara:", namaNegara);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this, "Check your connection network.\n" +
                    "and or your GPS status", Toast.LENGTH_SHORT).show();
            MainActivity.this.finish();
        }
        editText.setText(namaKota);
    }

    public static Location lastKnownLocation(Context context) {
        Location location = null;
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List list = locationManager.getAllProviders();
        boolean b = false;
        Iterator iterator = list.iterator();
        do {
            if (!iterator.hasNext()) break;
            String s = (String) iterator.next();
            if (b && !locationManager.isProviderEnabled(s)) continue;
            Location lastKnownLocation = locationManager.getLastKnownLocation(s);
            if (lastKnownLocation == null) continue;
            else {
                float v = location.getAccuracy();
                float v1 = lastKnownLocation.getAccuracy();
                if (v >= v1) {
                    long l = location.getTime();
                    long l1 = lastKnownLocation.getTime();
                    if (l - l1 <= 600000L) continue;
                }
            }
            location = lastKnownLocation;
            b = locationManager.isProviderEnabled(s);
        } while (true);
        return location;
    }
}
